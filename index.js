// console.log("Last session of the week");

// [Section] While Loop
    /* 
        A while loop takes in an expression/condition.
        Expressions are any unit of code that can be evaluated to a value.
        If the condition evaluates to be true, the statements inside the code block will be executed.
        A llop will iterate a certain number of times until and expression / condition is met.
    */

    /* let count = 5;

    while(count!==0) {
        console.log("While: " + count);
        count--;
        console.log("Value of count after the iteration: " + count);
    } */

// [Section] Do While Loop

    // works a lot like the while loop but unlike while loop, do-while loops guarantee that the code will be executed at least once.

    /* let number = Number(prompt("Give me a number: ")) ;

    do {
        console.log("Do while: " + number);
        number++;
    } while (number<10); */

// [Section] For loop
    /* 
        A for loop is more flexible than while and do-while.
        it consists of 3 parts :
        1. The "initialization" value that will track the progression of the loop.
        2. The "expression/condition" that will be evaluated which will determine whether the loop will run one more time.
        3. The "final expression" indicates how to advance the loop.
    */

    /* for (let count=0; count<=20; count++) {
        console.log("The current value of count is: " + count);
    } */

    // String - For Loop
    let myString = "alex";
    
    /* console.log(myString.length);
    console.log(myString[0]);
    console.log(myString[1]);
    console.log(myString[2]);
    console.log(myString[3]); */

    /* for (let i=0; i<myString.length; i++) {
        console.log(myString[i]);
    } */

    /* let numA = 15;

    for (let i=0; i<=10; i++) {
        let exponential = numA**i;
        console.log(exponential);
    } */

    /* let myName = "Alex";

    for(let i=0; i<myName.length; i++) {
        if (isVowel(myName[i].toLowerCase())) {
            console.log("3");
        } else {
            console.log(myName[i]);
        }
    }

    function isVowel (char) {
        if (char === "a" ||
            char === "e" ||
            char === "i" ||
            char === "o" ||
            char === "u" ) {
                return true;
            } else {
                return false;
            }
    } */

// [Section] Continue and break statements
    /* 
        - the "continue" statements allows the code to go to the next iteration of the loop without finishing the execution of all the statements in a code block.
        - the "break" statement is used to terminate the current loop once a match has been found.
    */

        /* for (let count=0; count<=20; count++) {
            if (count%2 === 0) {
                continue;
            } else if (count>10) {
                break;
            }
            console.log("Continue and Break: " + count);
        } */

        let name1 = "alexandro";

        for (let i=0; i<name1.length; i++) {
            if (name1[i].toLowerCase() === "a") {
                console.log("Continue to next iteration");
                continue;
            } 
            
            if (name1[i].toLowerCase() === "d") {
                console.log("Console log before the break");
                break;
            }

            console.log(name[i]);
        }